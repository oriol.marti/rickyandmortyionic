import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})
export class DetalleComponent implements OnInit {
  @Input() id ;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  regresar(){
    this.modalCtrl.dismiss()
  }

  
 
 
}
