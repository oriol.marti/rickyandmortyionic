import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-detalle-locations',
  templateUrl: './detalle-locations.component.html',
  styleUrls: ['./detalle-locations.component.scss'],
})
export class DetalleLocationsComponent implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  regresar(){
    this.modalCtrl.dismiss()
  }

  
}
