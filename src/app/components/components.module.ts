import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { DetalleComponent } from './detalleCharacters/detalle.component';


@NgModule({
  declarations: [
    DetalleComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    DetalleComponent
  ]
})
export class ComponentsModule { }
