import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { EMPTY } from 'rxjs';
import { DetalleComponent } from 'src/app/components/detalleCharacters/detalle.component';
import { Character } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';
import { DataLocalService } from '../../services/data-local.service';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  contador = 0;
  characters: Character[] = [];
  searchTerm: String;
  /* searchdata =  (<HTMLInputElement>document.getElementById("searchbar")).value; */
  
  constructor(private http: HttpClient, public dataService: DataService, private modalCtrl: ModalController ,private dataLocalService: DataLocalService) { }
  
  buscador(inputdata){
    document.write(inputdata);
    var tdata = inputdata.split(' ').join('+');
    return tdata
    //this.dataService.getSearchChar(tdata)
  }

  ionViewWillEnter(){
      this.dataLocalService.setData;
  }

  refreshPage() {
  this.ionViewWillEnter();
  }

  loadCharacters(event?) {
/*     if(this.buscador.length > 0){ */
      this.dataService.getCharacters().subscribe(
        resp => {
          console.log('Characters', resp);
          if (resp.info.next === null) {
            event.target.disabled = true;
            event.target.complete();
            return;
          }
          this.characters.push(...resp.results);
          console.log(resp.info.next);
          if (event) {
            event.target.complete();
          }
        });
/*     }else{
      this.dataService.getSearchChar(this.buscador).subscribe(
        resp => {
          console.log('Characters', resp);
          if (resp.info.next === null) {
            event.target.disabled = true;
            event.target.complete();
            return;
          }
          this.characters.push(...resp.results);
          console.log(resp.info.next);
          if (event) {
            event.target.complete();
          }
        });
    } */
  }

  async verDetalle(name: string, species: string, gender: string, image: string){
    const modal = await this.modalCtrl.create({
      component: DetalleComponent,
      componentProps: { name, species, gender, image }
    })
    modal.present()
  }

  setInStorage(data: Character){
    this.dataLocalService.setData(data)
  }

  ngOnInit(): void {
    
    this.loadCharacters();
    
  }

  loadData(event) {
    this.loadCharacters(event);
    
  }

}
