import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DetalleLocationsComponent } from 'src/app/components/detalle-locations/detalle-locations.component';
import { Location } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  searchTerm2: String;
  locations: Location[] = [];

  constructor(private http: HttpClient, public dataService: DataService, private modalCtrl: ModalController) {}

  loadLocations(event?) {
    this.dataService.getLocations().subscribe(
      resp => {
        console.log('Locations', resp);
        if (resp === null) {
          event.target.disabled = true;
          event.target.complete();
          return;
        }
        this.locations.push(...resp.results);
        console.log(this.locations);
        if (event) {
          event.target.complete();
        }
      });
  }
  async verDetalle(name: string, id: string, type: string, dimension: string ){
    const modal = await this.modalCtrl.create({
      component: DetalleLocationsComponent,
      componentProps: { name, id, type, dimension }
    })
    modal.present()
  }
  ngOnInit(): void {
    this.loadLocations();
  }
  
  loadData(event) {
    this.loadLocations(event);
  }

}
