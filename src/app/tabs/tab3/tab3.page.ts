import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DetalleComponent } from 'src/app/components/detalleCharacters/detalle.component';
import { Character } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  arrayfav: Character[]=[]; 

  constructor(private http: HttpClient, public dataService: DataService, private modalCtrl: ModalController, private dataLocalService: DataLocalService) { }

  ionViewWillEnter(){
        this.ngOnInit();
  }

  refreshPage() {
    this.ionViewWillEnter();
  }

  async verDetalle(name: string, species: string, gender: string, image: string){
    const modal = await this.modalCtrl.create({
      component: DetalleComponent,
      componentProps: { name, species, gender, image }
    })
    modal.present()
  }
  deleteInStorage(data: Character){
    this.dataLocalService.deleteData(data)
  }
  
  async ngOnInit(){
    this.arrayfav = await this.dataLocalService.getData();

  }
  

}

