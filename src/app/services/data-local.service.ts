import { Injectable } from '@angular/core';
import { Character, Characters } from '../interfaces/interfaces';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  /* public storage: Storage | null = null;  */
  public charactera: Character[]=[]; 

  constructor( public storage: Storage ) {
   this.init()
  }

  async init() {
   const storage = await this.storage.create();
   this.storage = storage;
  }

  async setData(value){
    this.charactera.push(value);
    this.storage.set('fav', this.charactera)

  }
  async deleteData(value){
  this.charactera = this.charactera.filter(Character=>Character.id==value.id)
  this.storage.set('fav', this.charactera)

  }

 async getData(){
  const data = await this.storage.get('fav')
  return data
} 
 
}
