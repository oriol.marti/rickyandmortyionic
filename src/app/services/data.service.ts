import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Characters, Locations } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  contador= 0;

  constructor(private http: HttpClient) {
  }
    getCharacters(){this.contador++;
      return this.http.get<Characters>(`https://rickandmortyapi.com/api/character?page=${this.contador}`)
    }
    getLocations(){
      return this.http.get<Locations>(`https://rickandmortyapi.com/api/location`)
    }
/*     getSearchChar(variablechar){
      return this.http.get<Characters>(`https://rickandmortyapi.com/api/character/?name=${variablechar}`)
    } */

}
