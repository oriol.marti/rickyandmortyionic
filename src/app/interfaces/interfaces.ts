export interface Characters {
    info: InfoChar;
    results: Character[];
}

export interface Character {
    id: number;
    name: string;
    status: string;
    species: string;
    type: string;
    gender: string;
    origin: Origin;
    location: Location;
    image: string;
    episode: string[];
    url: string;
    created: Date;
}

export interface Origin {
    name: string;
    url: string;
}

export interface InfoChar {
    count: number;
    pages: number;
    next: string;
    prev?: any;
}

export interface Locations {
    info: InfoLoc;
    results: Location[];
}

export interface Location {
      id: number;
      name: string;
      type: string;
      dimension: string;
      residents: string[];
      url: string;
      created: Date;
}
 
export interface InfoLoc {
        count: number;
        pages: number;
        next: string;
        prev?: any;
}





